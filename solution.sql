
--[S05 ACTIVITY]

--1. 
    SELECT customerName FROM customers WHERE country = "Philippines";


--2.
    SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

--3.
    SELECT productName, MSRP from products WHERE productName = "The Titanic";

--4.
    SELECT firstName, lastName FROM employees WHERE email="jfirrelli@classicmodelcars.com";

--5.
    SELECT customerName FROM customers WHERE state IS NULL ORDER BY customerName ASC;

--6.
    SELECT firstName, lastName, email FROM employees WHERE lastName="Patterson" AND firstName="Steve";

--7.
    SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit>3000;

--8.
    SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";


--9.
    SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";


--10.
    SELECT DISTINCT country FROM customers ORDER BY country ASC;

--11.
    SELECT DISTINCT status FROM orders ORDER BY status ASC;


--12.
    SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada") ORDER BY customerName ASC;


--13.
    SELECT employees.firstName, employees.lastName, offices.city FROM employees
        JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";


--14.
    SELECT customers.customerName FROM customers
        JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName="Thompson" ORDER BY customerName ASC;


--15.
    --oders  customerNumber, orderNumber
    --orderdetails - orderNumber, productCode
    --products -productCode productName

    SELECT customers.customerName, products.productName FROM orders
        JOIN customers ON orders.customerNumber =  customers.customerNumber
        JOIN  orderdetails ON orders.orderNumber = orderdetails.orderNumber
        JOIN products ON orderdetails.productCode = products.productCode
        WHERE customers.customerName = "Baane Mini Imports";

--16.
     SELECT CONCAT(employees.lastName, ', ' ,employees.firstName) AS SalesRepresentative, customers.customerName, offices.country, payments.* FROM customers
        JOIN employees ON customers.salesRepEmployeeNumber =  employees.employeeNumber
        JOIN  offices ON employees.officeCode = offices.officeCode
        JOIN payments ON customers.customerNumber = payments.customerNumber
        WHERE employees.employeeNumber = customers.salesRepEmployeeNumber AND customers.country = offices.country AND employees.officeCode = offices.officeCode;
--17.
    SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;


--18.
    SELECT customerName FROM customers WHERE phone LIKE "%+81%";